import React, { Component } from "react";
import "./header.css";

export default class Header extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <a className="navbar-brand navbar-logo" href="#">Start Bootstrap</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto"></ul>
                    <span className="nav-item active">
                        <a className="nav-link text-white" href="#">Home <span className="sr-only">(current)</span></a>
                    </span>
                    <span className="nav-item">
                        <a className="nav-link text-white-50" href="#">About </a>
                    </span>
                    <span className="nav-item">
                        <a className="nav-link text-white-50" href="#">Services </a>
                    </span>
                    <span className="nav-item">
                        <a className="nav-link text-white-50" href="#">Contact </a>
                    </span>
                </div>
            </nav>
        )
    }
}